Title: Does RMS support child abuse?

---

### Clarifications to the used terms

RMS criticizes the term `child` quoting:

> the term "child" is used as a form of deception, since it includes teenagers of an age at which a large fraction of people are sexually active nowadays. People we would not normally call children.[2] -- RMS 2003

---

It has been alleged that RMS supports child abuse[1].

FIXME-DOCS(Krey): To be processed

### References
1. List of arguments agains RMS on geek feminism wiki hhttps://web.archive.org/web/20210323214136/https://geekfeminism.wikia.org/wiki/Richard_Stallman
2. https://stallman.org/archives/2003-may-aug.html#25%20May%202003%20()
3. https://stallman.org/archives/2006-mar-jun.html#05%20June%202006%20(Dutch%20paedophiles%20form%20political%20party)
4. https://stallman.org/archives/2012-jul-oct.html#15_September_2012_%28Censorship_of_child_pornography%29
5. https://stallman.org/archives/2012-nov-feb.html#04_January_2013_(Pedophilia)
6. https://stallman.org/archives/2018-jul-oct.html#23_September_2018_(Cody_Wilson)
7. https://stallman.org/archives/2019-jul-oct.html#14_September_2019_(Sex_between_an_adult_and_a_child_is_wrong)